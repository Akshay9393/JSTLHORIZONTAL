<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="testTags" uri="/WEB-INF/tag/test.tld" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello< World!</h1>
        <myTags:Header fontColor="blue">
            Schedule a java task to run at a certain time, and wrap it as a windows service
            Written by admin on August 21, 2009 — Leave a Comment [edit]
            Make a Java program that run at a certain time is not difficult just use java.util.Timer and java.util.TimerTask classes, here is the code.
        </myTags:Header>
        <testTags:simpleOne/>
        <br>
        <testTags:classicOne/>
    </body>
</html>